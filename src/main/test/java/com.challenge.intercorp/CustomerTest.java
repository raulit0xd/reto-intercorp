package com.challenge.intercorp;

import com.challenge.intercorp.controller.CustomerController;
import com.challenge.intercorp.model.dto.CustomerDTO;
import com.challenge.intercorp.model.dto.CustomerKpiDTO;
import com.challenge.intercorp.model.entity.Customer;
import com.challenge.intercorp.model.repository.CustomerRepository;
import lombok.extern.log4j.Log4j2;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

@Log4j2
@RunWith(SpringRunner.class)
@SpringBootTest
public class CustomerTest {
    @Autowired
    private CustomerController customerController;
    @MockBean
    private CustomerRepository customerRepository;
    private   List<Customer> customerList;
    @Before
    public void initMocks() {
        Customer customer = Customer.builder().age(13).bornedDate(new Date()).customerId(1L)
                .name("Test Name").lastName("Test Last Name").build();

        customerList = new ArrayList<>();
        customerList.add(customer);
        Mockito.when( customerRepository.findAll())
                .thenReturn(customerList);
    }

    @Test
    public void listCustomersTest(){

        ResponseEntity<List<CustomerDTO>> listResponseEntity = customerController.listCustomers();
        log.info("Respuesta {}", listResponseEntity.getBody());
        assertEquals("No es igual", HttpStatus.OK, listResponseEntity.getStatusCode());
    }

    @Test
    public void getKpiTest(){
        ResponseEntity<CustomerKpiDTO> responseEntity = customerController.getKpis();
        log.info("Respuesta {}", responseEntity.getBody());
        assertEquals("No es igual", HttpStatus.OK, responseEntity.getStatusCode());
    }


}
