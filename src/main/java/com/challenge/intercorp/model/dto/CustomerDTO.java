package com.challenge.intercorp.model.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
@Builder
public class CustomerDTO {

    @ApiModelProperty(hidden = true)
    private Long idCliente;

    @NotNull(message="El campo nombre no puede ser vacio")
    @Size(min=2, max=40)
	@ApiModelProperty(required = true)
	private String nombre;

    @NotNull(message="El campo apellido no puede ser vacio")
    @Size(min=2, max=40)
	@ApiModelProperty(required = true)
	private String apellido;

    @NotNull(message="El campo edad no puede ser vacio")
    @Min(value=1, message="La edad debe ser > 0")
    @Max(value=130, message="La edad debe ser < 130")
	@ApiModelProperty(required = true)
	private Integer edad;

    @NotNull(message="El campo fechaNacimiento no puede ser vacio")
	@ApiModelProperty(notes = "dd/MM/yyyy", required = true)
	private String fechaNacimiento;
	
	private String fechaProbableMuerte;


	
}
