package com.challenge.intercorp.model.dto;

import lombok.Data;

import java.math.BigDecimal;

@Data
public class CustomerKpiDTO {

	private BigDecimal averageAge;
	private BigDecimal standardDeviationAge;


	
}
