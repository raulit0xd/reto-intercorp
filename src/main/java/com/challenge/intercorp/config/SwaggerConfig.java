package com.challenge.intercorp.config;

import java.util.ArrayList;
import java.util.Collections;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	
    @Bean
    public Docket api() {
    	
        return new Docket(DocumentationType.SWAGGER_2)  
          .select()                                  
          .apis(RequestHandlerSelectors.basePackage("com.challenge.intercorp.controller"))
          .paths(PathSelectors.ant("/*"))                          
          .build()
          .apiInfo(apiInfo());                                           
    }
    Contact contact = new Contact(
            "Raúl Caja",
            "",
            "r_caja@yahoo.com"
    );


    private ApiInfo apiInfo() {
        return new ApiInfo(
          "Reto Indigital",
          "Reto indigital",
          "1.0", 
          "Ninguno", 
           contact,
          "License of API", "API license URL", new ArrayList<>());
    }
}
