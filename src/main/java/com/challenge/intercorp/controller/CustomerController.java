package com.challenge.intercorp.controller;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import com.challenge.intercorp.model.dto.CustomerDTO;
import com.challenge.intercorp.model.dto.CustomerKpiDTO;
import com.challenge.intercorp.service.CustomerService;

import io.swagger.annotations.ApiOperation;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import springfox.documentation.annotations.ApiIgnore;

import javax.validation.Valid;

@Log4j2
@RestController
public class CustomerController {
	
	@Autowired
	private CustomerService customerService;


	@GetMapping("/listclientes")
	@ApiOperation(value = "Listado de Clientes (incluye fecha probable de muerte ).", response = List.class)
	public ResponseEntity<List<CustomerDTO>> listCustomers() {
		List<CustomerDTO> lista = new ArrayList<>();
		try {
			lista = customerService.listCustomers();
		} catch (Exception e) {

			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(lista);

		}
		return ResponseEntity.ok(lista);
	}

	@GetMapping("/kpideclientes")
	@ApiOperation(value = "Mostrar el promedio  y desviación standard de la" +
			" edad de los clientes", response = CustomerKpiDTO.class)
	public ResponseEntity<CustomerKpiDTO> getKpis() {

		CustomerKpiDTO customerKpiDTO = new CustomerKpiDTO();

		try {

			customerKpiDTO = customerService.getKpi();

		} catch (Exception e) {

			log.error(e.getMessage());
			return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body(customerKpiDTO);
		}

		return ResponseEntity.ok(customerKpiDTO);
	}


	@RequestMapping(value = "/creacliente", method = RequestMethod.POST)
	@ApiOperation(value = "Creación de nuevo cliente")
	public ResponseEntity<Object> saveCustomer(@RequestBody  @Valid CustomerDTO customerDTO
	 ) {

		try {
			customerService.saveCustomer(customerDTO);

		} catch (Exception e) {

			return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
		}

		return new ResponseEntity<>(HttpStatus.OK);
	}
}
