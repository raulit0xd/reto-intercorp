package com.challenge.intercorp.service;

import java.util.List;
import java.util.stream.Collectors;

import com.challenge.intercorp.model.dto.CustomerDTO;
import com.challenge.intercorp.model.dto.CustomerKpiDTO;
import com.challenge.intercorp.model.entity.Customer;
import com.challenge.intercorp.model.repository.CustomerRepository;
import com.challenge.intercorp.util.Utility;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CustomerServiceImpl implements CustomerService {

	@Autowired
	private CustomerRepository customerRepository;

	@Override
	public List<CustomerDTO> listCustomers() {
		List<Customer> customers = (List<Customer>) customerRepository.findAll();

		return customers.stream().map(c -> CustomerDTO.builder().idCliente(c.getCustomerId())
				.nombre(c.getName().toUpperCase())
				.apellido(c.getLastName().toUpperCase()).edad(c.getAge())
				.fechaNacimiento(Utility.formatDateToString(c.getBornedDate()))
				.fechaProbableMuerte(Utility.getProbablyDateOfDeath(c.getBornedDate())).build())
				.collect(Collectors.toList());

	}
	@Override
	public CustomerKpiDTO getKpi(){
		CustomerKpiDTO customerKpiDTO = new CustomerKpiDTO();
		if(!listCustomers().isEmpty()) {
			List<Integer> listCustomerAge = listCustomers().stream().map(CustomerDTO::getEdad).collect(Collectors.toList());
			customerKpiDTO.setAverageAge(Utility.getAverageAge(listCustomerAge));
			customerKpiDTO.setStandardDeviationAge(Utility.getStandardDeviaton(listCustomerAge));
		}
		return customerKpiDTO;
	}

	@Override
	public void saveCustomer(CustomerDTO customerDTO ) throws Exception {

		Customer customer =Customer.builder().name(customerDTO.getNombre()).bornedDate(
				Utility.convertirStringAdate(customerDTO.getFechaNacimiento())).
				lastName(customerDTO.getApellido()).age(customerDTO.getEdad()).build();
		customerRepository.save(customer);
	}





	

}
