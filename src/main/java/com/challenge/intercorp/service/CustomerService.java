package com.challenge.intercorp.service;

import java.util.List;

import com.challenge.intercorp.model.dto.CustomerDTO;
import com.challenge.intercorp.model.dto.CustomerKpiDTO;
import com.challenge.intercorp.model.entity.Customer;

public interface CustomerService {
	
	List<CustomerDTO> listCustomers() ;
	CustomerKpiDTO getKpi();
	void saveCustomer(CustomerDTO customerDTO ) throws Exception;

	
}
