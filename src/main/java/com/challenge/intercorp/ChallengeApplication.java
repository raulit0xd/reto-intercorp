package com.challenge.intercorp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

@SpringBootApplication
@EnableSwagger2
@EnableJpaRepositories(basePackages = "com.challenge.intercorp.model.repository")
@EntityScan(basePackages = "com.challenge.intercorp.model")
public class ChallengeApplication {
	
    public static void main( String[] args ) {
    	
    	SpringApplication.run(ChallengeApplication.class, args);
    }
}
