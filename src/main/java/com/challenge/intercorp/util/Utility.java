package com.challenge.intercorp.util;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

public class Utility {
	
	private static final int ESPERANZA_VIDA = 75;
	private static final String FORMATO_FECHA = "dd/MM/yyyy";

	public static BigDecimal getAverageAge(List<Integer> listCustomerAge) {
		return new BigDecimal(listCustomerAge.stream().mapToInt(p->p).average().getAsDouble());
	}

	public static BigDecimal getStandardDeviaton(List<Integer> table) {
		BigDecimal mean = getAverageAge(table);
		double temp = 0;

		for (Integer i = 0; i < table.size(); i++) {
			BigDecimal val = new BigDecimal(table.get(i));
			double squrDiffToMean = Math.pow(val.subtract(mean).doubleValue(), 2);
			temp += squrDiffToMean;
		}
		double meanOfDiffs = (double) temp / (double) (table.size());
        BigDecimal bd = new BigDecimal(Math.sqrt(meanOfDiffs));
		return bd.setScale(2, RoundingMode.HALF_UP);
	}

	public static String formatDateToString(Date fecha) {
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMATO_FECHA);
		String fechaConFormato = simpleDateFormat.format(fecha);
		return fechaConFormato;
	}
	
	public static Date convertirStringAdate(String fechaString) throws Exception {
		
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(FORMATO_FECHA);
		
		Date fecha;
		
		try {
			
			fecha = simpleDateFormat.parse(fechaString);
			
		} catch (ParseException e) {
			
			throw new Exception(String.format("La fecha debe tener el formato %s", FORMATO_FECHA));
		}
		
		return fecha;
	}

	public static Date getMaxDateofDeath(Date fecha, int anios) {
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(fecha);
		calendar.add(Calendar.YEAR, anios);
		return calendar.getTime();
	}

	public static String getProbablyDateOfDeath(Date bornedDate) {

		Date maxDateOfDeath = getMaxDateofDeath(bornedDate, ESPERANZA_VIDA);
		Date probablyDateOfDeath = new Date(ThreadLocalRandom.current().nextLong(new Date().getTime(),
				maxDateOfDeath.getTime()));
		return formatDateToString(probablyDateOfDeath);
	}



}
