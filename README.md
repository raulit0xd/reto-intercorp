

Herramientas Utilizadas:
Java 8, Maven,Hibernate, JUnit, RDS Mysql en  AWS
y para el despliegue en AWS utilice eastic bean stalk .


URL SWAGGER
-----------
http://restapichallenge-env.eba-3kybj2qf.us-west-2.elasticbeanstalk.com/swagger-ui.html


Métodos:
-----------

GET
http://restApiChallenge-env.eba-3kybj2qf.us-west-2.elasticbeanstalk.com/listclientes

GET
http://restApiChallenge-env.eba-3kybj2qf.us-west-2.elasticbeanstalk.com/kpideclientes

POST
http://restApiChallenge-env.eba-3kybj2qf.us-west-2.elasticbeanstalk.com/creacliente 


{
  "apellido": "BALTAZAR",
  "edad": 23,
  "fechaNacimiento": "01/03/1997",
  "nombre": "ENRIQUE"
}